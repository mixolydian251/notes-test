# A root level note

This is an example note written in markdown to test a feature of an application.
This has an `inlined code` block. As well as *italics* and a bit of **bold**.

This is another paragraph.

* some items
* in a
* list

1. and a
2. numbered
3. list

```
a code
block
```

## h2

### h3

#### h4

##### h5

